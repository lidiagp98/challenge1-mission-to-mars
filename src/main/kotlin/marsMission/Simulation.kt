package marsMission

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL

class Simulation {
    //Returns a list of the items that are read from the text file in the given URL
    fun loadItems(url: String): ArrayList<Item>{
        val items = arrayListOf<Item>()
        val fileURL = URL(url)
        //Opens the connection and reads the information
        val buffer = BufferedReader(InputStreamReader(fileURL.openStream()))
        buffer.forEachLine { line ->
            if (line.any()) {   //Check if the line has data
                //Divide the items by its name and weight
                val l = line.split("=")
                //Creating an item with the information and converting weight in kg to weight in tons
                val item = Item(l.first(),l.last().toDouble().div(1000))
                items.add(item)
            }
        }
        return items
    }

    //Returns a list of U1 rockets with items loaded
    fun loadU1(loadedItems: ArrayList<Item>): ArrayList<U1>{
        val u1Rockets = arrayListOf<U1>()
        val pendingItems = arrayListOf<Item>()  //A list that will handle the items that don't fit in the current rocket
        while (loadedItems.size > 0){           //Check if are any items left to load in a rocket
            val u1 = U1()
            loadedItems.forEach { item ->
                if (u1.canCarry(item)){         //If the actual rocket can carry the item it is loaded
                    u1.carry(item)
                }
                else pendingItems.add(item)     //Otherwise it is added to the list of remaining items to load
            }
            loadedItems.clear()                 //Clear the list so in case there are no items left it will stop the loop
            if (pendingItems.size > 0) {        //Check if there are any remaining items
                loadedItems.addAll(pendingItems)    //Load the remaining items to the list so they can be added in a new rocket
            }
            u1Rockets.add(u1)                   //Save the current rocket
            pendingItems.clear()                //Clear the list to avoid duplicates
        }
        return u1Rockets
    }

    //Returns a list of U2 rockets with items loaded
    fun loadU2(loadedItems: ArrayList<Item>): ArrayList<U2>{
        val u2Rockets = arrayListOf<U2>()
        val pendingItems = arrayListOf<Item>()  //A list that will handle the items that don't fit in the current rocket
        while (loadedItems.size > 0){           //Check if are any items left to load in a rocket
            val u2 = U2()
            loadedItems.forEach { item ->
                if (u2.canCarry(item)){         //If the actual rocket can carry the item it is loaded
                    u2.carry(item)
                }
                else pendingItems.add(item)     //Otherwise it is added to the list of remaining items to load
            }
            loadedItems.clear()                 //Clear the list so in case there are no items left it will stop the loop
            if (pendingItems.size > 0) {        //Check if there are any remaining items
                loadedItems.addAll(pendingItems) //Load the remaining items to the list so they can be added in a new rocket
            }
            u2Rockets.add(u2)                   //Save the current rocket
            pendingItems.clear()                //Clear the list to avoid duplicates
        }
        return u2Rockets
    }

    //Implementing extension function to activate the shield
    fun Rocket.addShield(){
        hasShield = true
    }

    //Send the rockets to mars! Returns the total budget spend in the fleet send and prints a log of the launches
    fun<T: Rocket> runSimulation (rockets: ArrayList<T>): Double{
        var budget = 0.0
        var crashes = -rockets.size //Its initialized in negative so when its called in the loop it doesn't add trash counts
        var explosions = 0
        var launchedRockets = 0
        var flag: Int               //Flag that will determine if the rocket really crashed
        rockets.forEach { rocket ->
            flag = crashes+1        //We initialized it with the expected value of crashes after the loop
            do {
                budget+=rocket.rocketCost       //Add the cost of the current rocket to be sent                
                crashes++                       //It will sum up for every rocket in the list that's why it starts with -size
                //If crashes is different of what we expect it means a crash occurred
                if (flag!=crashes) println("Oh no! Rocket #${launchedRockets} crashed! Restarting launching...")
                launchedRockets++               //Sum 1 to the count of rockets sent
                while (!rocket.launch()){       //If the rocket exploded we will send another one in replacement until one launches with no problem
                    budget+=rocket.rocketCost   //Sum up the cost of the new rocket
                    explosions++                //Register the explosion
                    println("Oh no! Rocket #${launchedRockets} exploded! Restarting launching...")
                    launchedRockets++           //Sum up the new rocket to the total of rockets
                }
            }while (!rocket.land())             //If the rocket that launches crashes it will launch again the rocket
            //If the rocket number is divisible by 3 it needs to have a shield
            if(launchedRockets.rem(3)==0){
                rocket.addShield()
            }
            if (rocket.hasShield) println("Rocket #${launchedRockets} with shield landed in Mars successfully!")
            else println("Rocket #${launchedRockets} landed in Mars successfully!")
        }
        println("---------REPORT---------")
        println("Number of explosions: $explosions \nNumber of crashes: $crashes \n" +  //Print the log to the console
                "Original Number of rockets: ${rockets.size} \n" +
                "Real total of rockets launched: $launchedRockets \n" +
                "Number of rockets with shields: ${rockets.sumBy { if (it.hasShield) 1 else 0}}")
        return budget
    }
}