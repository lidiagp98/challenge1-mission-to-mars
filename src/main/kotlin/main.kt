import marsMission.Item
import marsMission.Simulation
import java.lang.Exception
import kotlin.math.abs

fun main(args: Array<String>) {
    val simulation = Simulation()
    try {
        //List of the budgets needed for each simulation, in order: phase1 u1, phase 2 u1, phase 1 u2, phase 2 u2
        var budgets = arrayListOf<Double>()
        var itemsP1 = simulation.loadItems("https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d67_phase-1/phase-1.txt")
        var u1Rockets = simulation.loadU1(itemsP1)
        println("\n**** Welcome to our Mission to Mars Simulator! ****\n")
        println("**PHASE 1 SIMULATION - U1 ROCKETS**")
        budgets.add(simulation.runSimulation(u1Rockets).div(1000000))
        println("Budget: ${budgets[0]} Million ($)")

        var itemsP2 = simulation.loadItems("https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d88_phase-2/phase-2.txt")
        u1Rockets = simulation.loadU1(itemsP2)
        println("\n**PHASE 2 SIMULATION - U1 ROCKETS**")
        budgets.add(simulation.runSimulation(u1Rockets).div(1000000))
        println("Budget: ${budgets[1]} Million ($)")

        itemsP1 = simulation.loadItems("https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d67_phase-1/phase-1.txt")
        var u2Rockets = simulation.loadU2(itemsP1)
        println("\n**PHASE 1 SIMULATION - U2 ROCKETS**")
        budgets.add(simulation.runSimulation(u2Rockets).div(1000000))
        println("Budget: ${budgets[2]} Million ($)")

        itemsP2 = simulation.loadItems("https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d88_phase-2/phase-2.txt")
        u2Rockets = simulation.loadU2(itemsP2)
        println("\n**PHASE 2 SIMULATION - U2 ROCKETS**")
        budgets.add(simulation.runSimulation(u2Rockets).div(1000000))
        println("Budget: ${budgets[3]} Million ($)")

        println("\n**RECOMMENDATIONS**")
        println("For PHASE 1 is more efficient to use ${if (budgets[0]<budgets[2]) "U1 rockets" else "U2 rockets"}" +
                " with a difference of: ${abs(budgets[0]-budgets[2])}  Million ($)")
        println("For PHASE 2 is more efficient to use ${if (budgets[1]<budgets[3]) "U1 rockets" else "U2 rockets"}" +
                " with a difference of: ${abs(budgets[1]-budgets[3])} Million ($)")
    }catch (e: Exception){
        println("There was an error loading the items, check the source and try again")
    }
}