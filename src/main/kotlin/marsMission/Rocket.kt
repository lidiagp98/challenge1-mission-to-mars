package marsMission

open class Rocket(
    val rocketCost: Double,                     //Monetary cost of the rocket in dollars
    val rocketWeight: Double,                   //Total weight of the rocket alone in Tons
    val maxWeight: Double                       //Total weight the rocket can carry safety in tons
) : SpaceShip{
    var chanceOfExplosion: Double = 0.0         //Probability that the rocket has to explode in launching
    var chanceOfCrash: Double = 0.0             //Probability that the rocket has to crash in landing
    var currentWeight: Double = rocketWeight    //Total weight in Tons that the rocket is carrying at a certain moment
    var hasShield: Boolean = false              //Determines whenever a rocket has a energy shield or not
    override fun launch(): Boolean = true       //Returns true if the rocket launched safety false if it exploded
    override fun land(): Boolean = true         //Returns true if the rocket landed safety false if it crashed
    override fun canCarry(item: Item): Boolean = currentWeight+item.weight <= maxWeight     //Returns true if the rocket can carry certain item
    override fun carry(item: Item) {            //Adds the item weight to the current weight in the rocket (loads the item in the rocket)
        if (canCarry(item)){
            currentWeight += item.weight
        }
    }
}
//Put the default values for the U1 type of rockets
class U1: Rocket(100000000.00,10.0,18.0){
    //Constants to calculate the chance of explosion and crashing for U1 Rockets
    companion object{
        const val BASE_EXPLOSION = 0.05
        const val BASE_CRASHING = 0.01
    }
    //Calculates the chance of explosion for U1 rockets and stores it in percentage
    override fun launch(): Boolean {
        chanceOfExplosion = (BASE_EXPLOSION*(currentWeight/maxWeight))*100
        //If a random number is lower or equal to the chance of explosion it means the rocket exploded
        return (0..100).random() > chanceOfExplosion
    }
    //Calculates the chance of crashing for U1 rockets and stores it in percentage
    override fun land(): Boolean {
        chanceOfCrash = (BASE_CRASHING*(currentWeight/maxWeight))*100
        //If a random number is lower or equal to the chance of crashing it means the rocket crashed
        return (0..100).random() > chanceOfCrash
    }
}
//Put the default values for the U2 type of rockets
class U2: Rocket(120000000.00,18.0,29.0){
    //Constants to calculate the chance of explosion and crashing for U2 Rockets
    companion object{
        const val BASE_EXPLOSION = 0.04
        const val BASE_CRASHING = 0.08
    }
    //Calculates the chance of explosion for U2 rockets and stores it in percentage
    override fun launch(): Boolean {
        chanceOfExplosion = (BASE_EXPLOSION *(currentWeight/maxWeight))*100
        //If a random number is lower or equal to the chance of explosion it means the rocket exploded
        return (0..100).random() > chanceOfExplosion
    }
    override fun land(): Boolean {
        chanceOfCrash = (BASE_CRASHING *(currentWeight/maxWeight))*100
        //If a random number is lower or equal to the chance of crashing it means the rocket crashed
        return (0..100).random() > chanceOfCrash
    }
}